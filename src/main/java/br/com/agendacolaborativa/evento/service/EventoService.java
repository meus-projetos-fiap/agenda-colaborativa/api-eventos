package br.com.agendacolaborativa.evento.service;

import br.com.agendacolaborativa.evento.entity.Evento;
import br.com.agendacolaborativa.evento.repository.EventoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EventoService {

    @Autowired
    private EventoRepository eventoRepository;

    public void salvarEvento(Evento evento) {
        eventoRepository.save(evento);
    }

    public List<Evento> listarTodos() {
        return eventoRepository.findAll();
    }

    public long count() {
        return eventoRepository.count();
    }

    public Evento pesquisarEventoPorId(String id) {
        return eventoRepository.findByEventoId(id);
    }

    public List<Evento> pesquisarEventosPorUsuarioId(String usuarioId) {
        return eventoRepository.findByUsuarioId(usuarioId);
    }

    public void excluirEvento(String id) {
        eventoRepository.deleteById(id);
    }

}