package br.com.agendacolaborativa.evento.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;

@Document(collection = "eventos")

@Data
public class Evento {

    @Id
    @GeneratedValue
    private String eventoId;
    private String usuarioId;
    private String descricao;
    private String endereco;
    private String data;
    private String duracao;
    private boolean checkin;

    public boolean getCheckin() {
        return this.checkin;
    }
}
