package br.com.agendacolaborativa.evento.controller;

import br.com.agendacolaborativa.evento.entity.Evento;
import br.com.agendacolaborativa.evento.repository.EventoRepository;
import br.com.agendacolaborativa.evento.service.EventoService;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/eventos")
public class EventoController {

    final static Logger logger = LoggerFactory.getLogger(EventoController.class);

    @Autowired
    private EventoService eventoService;
    @Autowired
    private EventoRepository eventoRepository;

    @GetMapping
    public List<Evento> getAllEventos() {
        return eventoService.listarTodos();
    }

    @GetMapping("/usuario/{id}")
    public List<Evento> getAllEventosDoUsuario(@PathVariable String id) {
        return eventoService.pesquisarEventosPorUsuarioId(id);
    }

    @PostMapping
    public ResponseEntity<?> salvarEvento(@RequestBody Evento evento) {
        eventoService.salvarEvento(evento);
        return new ResponseEntity("Student added successfully", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deletarEvento(@PathVariable String id) {
        logger.info("Delete[" + id + "]");
        eventoService.excluirEvento(eventoService.pesquisarEventoPorId(id).getEventoId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Evento> atualizarEvento(@PathVariable String id, @RequestBody Evento evento) {
        logger.info("Atualizar[" + id + "]");
        logger.info("Atualizar[" + evento + "]");
        Evento _evento = eventoService.pesquisarEventoPorId(id);
        _evento.setDescricao(evento.getDescricao());
        _evento.setEndereco(evento.getEndereco());
        _evento.setData(evento.getData());
        _evento.setDuracao(evento.getDuracao());
        _evento.setCheckin(evento.getCheckin());
        eventoRepository.save(_evento);
        return new ResponseEntity<>(_evento, HttpStatus.OK);
    }

}
