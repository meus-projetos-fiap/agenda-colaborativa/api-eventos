package br.com.agendacolaborativa.evento.repository;

import br.com.agendacolaborativa.evento.entity.Evento;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EventoRepository extends MongoRepository<Evento, String> {

    Evento findByEventoId(String id);
    List<Evento> findByUsuarioId(String usuarioId);
}