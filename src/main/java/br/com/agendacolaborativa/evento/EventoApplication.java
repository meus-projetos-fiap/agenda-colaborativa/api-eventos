package br.com.agendacolaborativa.evento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class EventoApplication {
    private static final Logger logger = LoggerFactory.getLogger(EventoApplication.class);

    public static void main(String[] args) {
        logger.info("INI-001");
        SpringApplication.run(EventoApplication.class, args);
    }

}
